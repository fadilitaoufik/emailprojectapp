  /**
 * la classe Message qui contient le message à transmettre
 * @attr  object string : L'objet du message à extraire de la zone de texte
 * @attr  message string : le message à extraire de la zone de texte
 * @attr etat string : Normal ou Urgent

 */

class Message {

    constructor (objet,message,etat){
      this._objet = objet;
      this._message = message;
      this._etat = etat;
    }

    set objet  (objet)  { this._objet = objet}
    get objet  ()       { return this._objet}

    set message  (message)  { this._message = message}
    get message  ()       { return this._message}

    set etat  (etat)  { this._etat = etat}
    get etat  ()       { return this._etat}

}
 

/**
 * Ajoute de Firebase à l'application
 */

 const config = {

    apiKey: "AIzaSyBkmHRQwPvDAjbrHqbYCbNiwysG1n2EYcg",
    authDomain: "glsidtempreal.firebaseapp.com",
    databaseURL: "https://glsidtempreal.firebaseio.com",
    storageBucket: "glsidtempreal.appspot.com",
    messagingSenderId: "688833972513"
  
  };

  firebase.initializeApp(config);

/*
    Ref est  représente un emplacement spécifique dans la base de données et peut être utilisée pour lire 
    ou écrire des données dans cet emplacement de base de données.
*/

  const ref = firebase.database().ref();


/*
    Ce rappel sera déclenché lorsque un element (child) stocké dans la  base de donnée Firebase
*/

ref.on('child_added', snapshot => {
     const parent = snapshot.val();

     let msg = new Message(parent._objet, parent._message, parent._etat);
      const ligne = creerLigne(snapshot.key,msg.objet, msg.message, msg.etat);
     ajouterLigne(ligne);
});

/*
    Ce rappel sera déclenché lorsque un element (child) supprimé dans la  base de donnée Firebase
*/

ref.on('child_removed', snapshot => {
      const ligne = document.getElementById(snapshot.key);
      supprimerLigne(ligne);
});


/*
    Ajouter l'événement click au Checkbox[ID = selectAll]
*/

  const chkbx = document.getElementById("selectAll");  
  
  chkbx.addEventListener("click", () => { 
     const checkNames = document.getElementsByName("checkName");
     checkNames.forEach(node => {
       node.checked = !node.checked;
     })
  }, false);


/**
 * Ajouter l'événement click au Button[ID = btnAll]
*/
    
  const btnAll = document.getElementById("btnAll");
     btnAll.addEventListener("click", () => { 
       supprimerLesLignesSelectionnees();
     } , false);


  const btnAdd = document.getElementById("btnAdd");
      btnAdd.addEventListener("click", () => { 

        let obj = document.getElementById('objet').value;
        let msgTxt = document.getElementById('message').value;
        
        let etatCB = document.getElementsByName('entree');  
        let etat = ( (etatCB[0].checked == true) ? "normale" : "urgent");

        const msg = new Message(obj,msgTxt,etat);
        ref.push(msg);
        
        } , false);
 
/**
 * Ajoute une ligne à la fin du tbody de la table
 * @param HTMLTableRowElement : Elément de type tr prêt à être inséré dans le tableau
 */

function ajouterLigne(tr){

    let container = document.getElementById('rows');
    container.appendChild(tr);

}

/**
 * Crée un élément tr (ligne du tableau)
 * @param  id string : L'id du la ligne pour identifier la ligne ajouté
 * @param  object string : L'objet du message à extraire de la zone de texte
 * @param  message string : le message à extraire de la zone de texte
 * @param etat string : Normal ou Urgent
 * @return HTMLTableRowElement : Elément de type tr prêt à être inséré dans le tableau.
 */

function creerLigne(id,object, message = "", etat = ""){

    let tr = document.createElement("TR");
          tr.id=id;
    
            let td1 = document.createElement('TD');
              const input = document.createElement("input");
              input.type = "checkbox";
              input.name = "checkName";
            td1.appendChild(input); 

           tr.appendChild(td1); 
 

            let td2 = document.createElement('TD');
            td2.innerText=object;
    
           tr.appendChild(td2); 

            let td3 = document.createElement('TD');
            td3.innerText= message;
           tr.appendChild(td3); 

            let td4 = document.createElement('TD');
                let  btn = document.createElement("BUTTON");
                btn.className = "btn btn-danger "; 
                btn.addEventListener("click", e => {
                ref.child(tr.id).remove();
                //supprimerLigne(tr);
                }, false);
                let  spn = document.createElement("SPAN");
                    spn.className = "glyphicon glyphicon-remove";
                btn.appendChild(spn);
            td4.appendChild(btn); 
          tr.appendChild(td4); 

    if( etat == "urgent"){
          tr.className = "danger";
    };

    
  return tr;

}

/**
 * Supprime une ligne
 * @param tr HTMLTableRowElement : La ligne qu'on veut supprimer
 */
function supprimerLigne(tr){
      return tr.parentNode.removeChild(tr);
}

/**
 * Supprime toutes les lignes sélectionnées
 */

function supprimerLesLignesSelectionnees(){
     const domem = document.getElementsByName("checkName");

     for(let i=0 ,j = 0,lt = domem.length; i<lt;i++){
         if (domem[j].checked == true) ref.child(domem[j].parentElement.parentElement.id).remove();
         else j++;
    
      }

  }